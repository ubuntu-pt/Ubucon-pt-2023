---
title: "Apelo a propostas"
description: "Submeta a sua proposta até 25 de Agosto às 23:55:00 de Lisboa"
externalRedirect: https://hub.ubuntu-pt.org/apps/forms/s/TQkCj6wX5JSydjadfHFk4zYW
---

Redirectionando para o formulário de submissão...

[Pressione para redirigir manualmente](https://hub.ubuntu-pt.org/apps/forms/s/TQkCj6wX5JSydjadfHFk4zYW)
