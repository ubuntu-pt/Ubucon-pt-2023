---
title: Almoço
date: 2023-09-17T12:30:00+01:00
end: 2023-09-17T14:00:00+01:00
room: Onde preferirem
category: Gainsboro
featured: false # If it's true. This session will appear on main page.
lang: Português
speakers: # Speaker info
    - name: Todos
      bio: Vamos todos almoçar.
      launchpad: # link to launchpad.net profile
      github:  # link to github profile
      profile: # Speaker photo
      linkurl: # Other website link url
      linklabel: # Label for linkurl
---

Bom proveito!