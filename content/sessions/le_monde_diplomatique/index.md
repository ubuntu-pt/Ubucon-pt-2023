---
title: "Le Monde diplomatique: um caminho digital Livre"
date: 2023-09-17T10:50:00+01:00
end: 2023-09-17T12:30:00+01:00 #Fim da sessão
room: Sala 4.1.02 (60 pessoas) # Número da sala
#Escolhas Possíveis:
# Palco Principal
# Palco Secundário
# Sala de Workshops
# Zona de Podcasts
category: "#BEB8B2" # Categoria da apresentação
#Escolhas Possíveis:
# "#AEA79F" Warm Grey 100%: Inicio e fim
# "#BEB8B2" Warm Grey 80%: Apresentações
# "#D6D3CF" Warm Grey 50%: Wrokshops
# "#E6E4E2" Warm Grey 30%: Podcasts
# "#F6F6F5" Warm Grey 10%: Outros
lang: Português
featured: true # Se for true, esta sessão irá aparecer na página principal
speakers: # Informação dos oradores
    - name: Miguel Gomes
      bio: Antropólogo, músico e formador, é um evangelista do Software Livre. É utilizador de Ubuntu desde 2009, membro do Conselho Comunitário da Comunidade Ubuntu Portugal, sócio da ANSOL e participa no podcast Ubuntu Portugal desde 2021. Em vários projectos, associações e empresas por onde passa e na sua vida pessoal, tenta sempre aplicar soluções FLOSS ou recomendar o seu uso. Trabalha com o Le Monde diplomatique - edição portuguesa desde 2020.
      email: mgc.lude@proton.me
---

# Le Monde diplomatique: um caminho digital Livre

<p>Pode uma toalha ser mais poderosa do que a espada?<br>

Como é que a recusa do serviço militar obrigatório levou à criação do primeiro jornal Francês com presença na Internet?<br>
 
Fundado originalmente em França em 1954 e em Portugal em Abril de 1999, o <i>Le Monde diplomatique</i> é um jornal internacional, presente em 30 países e traduzido para 19 línguas diferentes e o primeiro jornal Francês a ter um sítio web, em 1995.<br>

Editado em Portugal pela Cooperativa Outro Modo, o <i>Le Monde diplomatique - edição portuguesa</i> é um mensário de tenacidade crítica e informação aprofundada, que procura aprofundar, analisar e debater temas fulcrais da actualidade, essenciais para a construção da democracia, longe da "espuma dos dias" e do mediatismo efémero.<br>

Recentemente e em linha com a sua filosofia editorial, tem feito um maior esforço para reforçar o uso de Software Livre na sua edição, em particular no "online" - um caminho que iniciou em 2006 e ainda não terminou.<br>
Que desafios se apresentam?<br>
Que soluções e ferramentas foram encontradas?<br>
Como podem o Software Livre e Ubuntu agilizar a edição e arquivo em linha de um jornal, mantendo o compromisso com os seus princípios éticos e comunitários? O que falta fazer?<br>
Que desafios se colocam para o futuro?<br>
Esta comunicação tentará ilustrar tudo isto.</p>
