---
title: "Workshop - Criar Web com a Ubuntu-pt"
description: "Prepara-te para contribuir para equipa de Web da Ubuntu-pt"
date: 2023-09-17T14:00:00+01:00 #Inicio da sessão
end: 2023-09-17T14:30:00+01:00 #Fim da sessão
room: Sala 4.1.02 (60 pessoas) # Número da sala
#Escolhas Possíveis:
# Palco Principal
# Palco Secundário
# Sala de Workshops
# Zona de Podcasts
category: "#D6D3CF" # Categoria da apresentação
#Escolhas Possíveis:
# "#AEA79F" Warm Grey 100%: Inicio e Fim
# "#BEB8B2" Warm Grey 80%: Apresentação
# "#D6D3CF" Warm Grey 50%: Wrokshop
# "#E6E4E2" Warm Grey 30%: Podcast
# "#F6F6F5" Warm Grey 10%: Outro
lang: Português
featured: true # Se for true, esta sessão irá aparecer na página principal
speakers: # Informação dos oradores
    - name: Diogo Constantino # Nome
      bio: Membro do Conselho Comunitário Ubuntu-pt, e Ubuntu Member # Sobre
      email: diogoconstanino@ubuntu-pt.org  # Email
---

Contribuir para a equipa de Web da Ubuntu-pt é uma forma simples de contribuírem para o Ubuntu em Portugal e de se iniciarem no desenvolvimento web.

A nossa equipa é composta por pessoas com níveis de conhecimento muito distintos, desde especialistas a principiantes, e gostamos de partilhar o nosso conhecimento para enriquecermo-nos mutuamente. Temos "developers", "devops", "copyrighters" e até aqueles que ainda estão a decidir o que querem fazer.

Todos aprendemos com todos! Todos contribuímos!

Nesta sessão pretendemos passar o conhecimento sobre o que precisam para contribuir para a equipa de Web da Ubuntu-pt e ajudarmos a preparar o vosso ambiente de desenvolvimento de Web, utilizando as ferramentas disponíveis em Ubuntu e outras distribuições de GNU/Linux.
