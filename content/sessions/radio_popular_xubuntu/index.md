---
title: "Radio Popular - Implementação de Xubuntu 22.04 Desktop"
date: 2023-09-17T09:30:00+01:00
end: 2023-09-17T10:00:00+01:00 #Fim da sessão
room: Sala 4.1.02 (60 pessoas) # Número da sala
#Escolhas Possíveis:
# Palco Principal
# Palco Secundário
# Sala de Workshops
# Zona de Podcasts
category: "#BEB8B2" # Categoria da apresentação
#Escolhas Possíveis:
# "#AEA79F" Warm Grey 100%: Inicio e fim
# "#BEB8B2" Warm Grey 80%: Apresentações
# "#D6D3CF" Warm Grey 50%: Wrokshops
# "#E6E4E2" Warm Grey 30%: Podcasts
# "#F6F6F5" Warm Grey 10%: Outros
lang: Português
featured: true # Se for true, esta sessão irá aparecer na página principal
speakers: # Informação dos oradores
    - name: Francisco Rocha
      bio: https://www.linkedin.com/in/francisco-rocha-67bbaa24/
---

Rádio Popular: um caso prático de Implementação de Xubuntu 22.04 Desktop em contexto empresarial.