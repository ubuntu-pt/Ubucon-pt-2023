---
title: "Hackaton: Vamos renovar o site da Ubuntu-pt"
description: "Vamos todos dar o pontapé de saída para o novo site da Ubuntu-pt, criado pela comunidade."
date: 2023-09-17T14:35:00+01:00
end: 2023-09-17T16:20:00+01:00 #Fim da sessão
room: Sala 4.1.02 (60 pessoas) # Número da sala
#Escolhas Possíveis:
# Palco Principal
# Palco Secundário
# Sala de Workshops
# Zona de Podcasts
category: "#D6D3CF" # Categoria da apresentação
#Escolhas Possíveis:
# "#AEA79F" Warm Grey 100%: Inicio e fim
# "#BEB8B2" Warm Grey 80%: Apresentações
# "#D6D3CF" Warm Grey 50%: Wrokshops
# "#E6E4E2" Warm Grey 30%: Podcasts
# "#F6F6F5" Warm Grey 10%: Outros
lang: Português
featured: true # Se for true, esta sessão irá aparecer na página principal
speakers: # Informação dos oradores
    - name: Diogo Constantino
      bio: Membro do Conselho Comunitário Ubuntu-pt, e Ubuntu Member
      email: diogoconstanino@ubuntu-pt.org  # Email
---

O site da Ubuntu-pt está bastante desactualizado. Com a experiência da criação do site da Ubucon Portugal 2023 e com as experiências que estamos a trocar com outras LoCo por todo o mundo, sabemos exactamente qual é a base tecnológica que queremos usar para um novo site.
Agora...falta o resto! - mas não há melhor sítio para começar do que na Ubucon Portugal 2023, dando a todos a oportunidade de contribuir.