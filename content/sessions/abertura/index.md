---
title: Abertura da Ubucon Portugal
date: 2023-09-17T09:00:00+01:00
end: 2023-09-17T09:20:00+01:00
room: Sala 4.1.02 (60 pessoas)
category: Gainsboro
featured: false # If it's true. This session will appear on main page.
lang: Português
speakers: # Speaker info
    - name: Equipa da Ubucon Portugal
      bio: Organizadores da Ubucon Portugal
      email: diogoconstanino@ubuntu-pt.org  # Email
      launchpad: # link to launchpad.net profile
      github:  # link to github profile
      profile: # Speaker photo
      linkurl: # Other website link url
      linklabel: # Label for linkurl
---

Bem vindo à segunda Ubucon Portugal!