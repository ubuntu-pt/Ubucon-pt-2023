---
title: Encerramento da Ubucon Portugal 2023
description: Palavras finais da organização da Ubucon Portugal 2023
date: 2023-09-17T16:25:00+01:00
end: 2023-09-17T16:50:00+01:00
room: Sala 4.1.02 (60 pessoas)
category: Gainsboro
featured: false # If it's true. This session will appear on main page.
lang: Português
speakers: # Speaker info
    - name: Equipa da Ubucon Portugal
      bio: Organizadores da Ubucon Portugal
      email: diogoconstanino@ubuntu-pt.org  # Email
      launchpad: # link to launchpad.net profile
      github:  # link to github profile
      profile: # Speaker photo
      linkurl: # Other website link url
      linklabel: # Label for linkurl
---

Obrigado, Aveiro!