---
title: "Convocatória à apresentação de propostas"
description: "Convite para propostas até: 25 de Agosto às 23:55:00"
---
---

# Propostas

Pode propor palestras, eventos (por exemplo: podcasts gravados ao vivo) e oficinas. Mais detalhes em baixo

# Datas importantes

- Período de apresentação de propostas já começou e irá até: {{< timedisplay datetime="2022-08-28T23:55:00+01:00" display="datetime" >}}


# Submeter uma (ou mais) proposta(s)
- Certifique-se de que lê as directrizes abaixo indicadas, antes de submeter a proposta;
- Utilizamos este [formulário para receber as suas propostas](https://hub.ubuntu-pt.org/apps/forms/s/TQkCj6wX5JSydjadfHFk4zYW).

Se quer apresentar a sua proposta, mas não tem a certeza de que os tópicos são adequados para esta conferência.
Pode já ter escrito a sua proposta, mas talvez queira saber se a sua proposta contém informação suficiente.

Se tiver dúvidas por favor, sinta-se à vontade para nos contactar usando o [formulário de contacto geral](/contact/geral) indicando a que proposta se refere (caso a proposta já tenha sido submetida), ou qual a ideia e quais as dúvidas que tem a respeito dessa proposta (por favor limite a uma proposta por cada contacto).

A equipa da Ubucon Portugal 2023 está disponível para ajudar os potenciais oradores a preparar e submeter propostas. Note que a equipa pode não estar disponível instantaneamente.


{{% accordion title="Direitos de autor da gravação e dos materiais da sua sessão" id="tab7" expanded="true" hidden="false" %}}

Cada sessão poderá ser gravada em vídeo, editada e publicada em linha com CC BY 4.0. Os materiais da sessão, como os diapositivos, também serão publicados no nosso sítio Web com a mesma licença. Certifique-se de que não inclui qualquer conteúdo protegido por direitos de autor que entre em conflito com a CC BY 4.0 ou conteúdos pagos.
Se pretender outra licença ou não puder gravar a sua sessão ou partilhar os seus materiais, informe-nos quando submeter a sua proposta.
{{% /accordion %}}

{{% accordion title="Orientações para a redacção da sua proposta" id="tab5" expanded="true" hidden="false" %}}

### Escolha o tipo de sessão
A Ubucon Portugal deste ano está a aceitar propostas nos seguintes tipos de sessões.
- Apresentações, com diferentes opções de duração:
  * 30 minutos;
  * 45 minutos;
  * 60 minutos.
- Oficinas
  * 60 minutos;
  * 120 minutos;
- Podcasts
  * 30 minutos;
  * 60 minutos;

### Língua da sessão e tipo de participação
- A língua da sessão será o Português.

### Escolha de tópicos
Pode escrever propostas com tópicos não listados abaixo, se estiverem relacionados com o Ubuntu ou tecnologias de Software Livre relacionadas, ou sobre como fazer algo utilizando Ubuntu (incluindo "sabores" oficiais, remisturas e derivados).

A Ubucon Portugal 2023 está a aceitar propostas com os seguintes tópicos (mas não apenas estes):

- Ambiente de trabalho ("Desktop")
  - GNOME, KDE, GTK, QT, Flutter, implementações de ambientes de trabalho empresariais, Landscape, Active Directory, sabores do Ubuntu, Pipewire, X.Org, Wayland, Mir, CUPS, OpenPrinting e muito mais.

- Nuvem ("Cloud") e infra-estrutura
  - Nuvem pública e privada, no local: NextCloud, AWS, Google Cloud, Azure, OpenStack, MicroStack, MAAS, imagens da Nuvem Ubuntu e muito mais;
  - Contentores Linux e orquestração de contentores: Docker, LXC, LXD, Kubernetes, Microk8s, imagens do Ubuntu Container e muito mais;
  - DevOps: Juju, cloud-init, Multipass, MAAS, CI/CD e outros;
  - Virtualização: KVM, QEMU, Xen; outros;
  - Automações, redes: Landscape, Netplan, cloud-init e outros;

- Subsistema Windows para Linux (WSL)
  - Ubuntu WSL & Interoperabilidade com Windows, WSL no Windows Server, Desenvolvendo para Linux usando WSL, Containers no WSL e outros;

- Localizações, internacionalizações e acessibilidade

- IoT, Embedded, Robótica, Appliances, Domótica
  - Ubuntu Core, Raspberry Pi, ROS, Snaps em IoT, Mir Display Server, Ubuntu Frame; outros.

- Empacotamento ("Packaging")
  - Empacotamento Debian (*.deb), empacotamento Snap (Snapcraft), Flatpak, AppImage; outros;
  - Operar repositórios de pacotes (usando um serviço hospedado como o Launchpad PPA ou hospedagem própria), operar servidores-espelho de pacotes ("mirror servers") entre outros;
  - Automatizar o empacotamento com Launchpad Build Recipes, CIs como GitHub Actions;

- Documentação, QA e triagem de "bugs"

- Segurança, Conformidade ("Compliance") e Núcleo ("Kernel")
  - Segurança: AppArmor, ufw, SMACK, SELinux, ESM (Extended Security Maintenance), Livepatch, CVEs e Avisos de Segurança do Ubuntu; outros;
  - Conformidade: FIPS, CIS, PCI-DSS, Licença de código aberto; outros;
  - Kernel: Livepatch, Variantes do Kernel para Ubuntu, Drivers de Dispositivos, Sistema de Arquivos; outros;
- Dados e IA: Ubuntu para cargas de trabalho de Ciência de Dados, Engenharia de Dados, IA e ML;

- Conteúdo e design
  - Edição de vídeo, áudio e imagem, design gráfico, gráficos 3D com código aberto no Ubuntu;
  - Ferramentas de código aberto como o Gimp, Inkscape, Blender, Krita, Scribus, Kdenlive, OpenShot, Audacity;
  - Outros.

- Comunidade, iniciativas comunitárias, diversidade, alcance local e contexto social.

### Título da sessão e resumos
Dê-nos uma visão geral e pormenorizada da sua sessão. O título deve ser conciso e apresentar o tópico principal e o resumo deve conter uma visão geral pormenorizada da sessão. Também pode incluir uma breve ordem de trabalhos no resumo da mesma.

#### Escolha de tópicos
- Por favor, note que este é um evento Ubuntu. Os tópicos relacionados com o Ubuntu terão uma prioridade mais alta. Por favor, sugira um tópico que esteja pelo menos minimamente relacionado com o Ubuntu e com as tecnologias relacionadas com o ecossistema Ubuntu ou a comunidade Ubuntu.
- Por favor, seja específico! Tópicos como "O que é o Ubuntu?", "O que é segurança", "O que é engenharia social" são demasiado abrangentes. Na maioria dos casos, este tipo de tópicos pode ser rejeitado.
- Tópicos genéricos como "Gestão de Incidentes", "Gerir o stress", "Criar o seu plano estratégico pessoal", "Dinâmica de Equipas & Competências de Comunicação" podem ser adequados para palestras públicas, mas normalmente não são adequados para o nosso evento. Se relacionar tópicos genéricos com projectos ou comunidade Ubuntu, isso pode ser adequado. Por exemplo, falar apenas de "impostos e contabilidade fiscal" não é adequado para o nosso evento, mas se falar da sua experiência na gestão de contabilidade e impostos para a UbuCon ou outras actividades da comunidade Ubuntu? Isso seria um tópico interessante para outros utilizadores de Ubuntu!

- Por favor, evite sugerir propostas que se limitem a fornecer informações fáceis de encontrar, especialmente propostas que revelem fortes intenções comerciais ou publicitárias.
   - Muitas audiências assistem às apresentações por causa da reputação ou experiência do apresentador ou para beneficiarem de informação nova ou difícil de encontrar - em vez de informações que podem ser facilmente encontradas através de uma pesquisa em linha.
   - São bem-vindas as experiências e casos únicos; estudos de caso de utilização do Ubuntu ou de fontes abertas relacionadas com a sua empresa, conteúdos relacionados com o Ubuntu ou fontes abertas relacionadas que sejam úteis para o público.
- Se quiser encontrar referências, pode dar uma vista de olhos em https://wiki.ubuntu.com/Ubucon, para sessões em UbuCons anteriores.

#### Bons e maus exemplos

**Vejamos alguns maus exemplos primeiro.

>Instalação do Ubuntu
>
>Vamos instalar o Ubuntu hoje!

Porque é que é uma má proposta?
- Este tópico é demasiado abrangente. Existem muitas variantes do Ubuntu; que variante? Em que tipo de máquina? Ubuntu Desktop em computadores portáteis ThinkPad? Ubuntu em Raspberry Pi? Que arquitectura? ARM ou AMD64? É vantajoso ser mais específico.
- Instalar o Ubuntu é algo que se pode encontrar facilmente através de uma pesquisa na Web. Este tema não é muito adequado para o nosso evento.
- O resumo não fornece informações suficientes. Apenas diz `Vamos instalar o Ubuntu hoje!` e isso é tudo. A maioria das pessoas não tem uma ideia mais concreta do que vai ser apresentado.

>Hackeando o UnityX e criando remixes do Ubuntu
>
>Primeiramente, irei mostrar como configurar um ambiente de compilação para o UnityX. em seguida, mostrarei como fazer um Merge Request no GitLab e como empacotar o UnityX usando o qckdeb. Lançámos recentemente o nosso ISO builder, para que qualquer pessoa o possa usar para criar ISOs do Ubuntu Remix. Irei demonstrar quão fácil é usar o "builder", através da criação de um Ubuntu Remix live, usando o UnityX.

Porque é que é uma boa proposta?
- Título conciso e específico e resumo detalhado para que os presentes saibam o que vão aprender com a sessão.
- A sessão não fala apenas de .Net, mas também se relaciona com alguns tópicos do Ubuntu, como imagens de contentores do Ubuntu. O que torna esta sessão adequada para o nosso evento.

### Conhecimentos prévios e o que os participantes precisam de preparar
Há algum conhecimento prévio que o público deva possuir para compreender a sua sessão? Por favor, especifique se for o caso. Se a sua sessão for uma oficina e os participantes tiverem de preparar algo antes de participarem, como a instalação de software, a inscrição num serviço em linha ou a entrega de um equipamento específico, certifique-se de que especifica na sua proposta atempadamente.

### O que é que o público pode aprender com a sua sessão
Porque é que os participantes terão de participar na sua sessão? O que é que o público pode aprender com a sua sessão? Pense em alguns pontos de atractividade da sua sessão e especifique-os na sua proposta.

### Outras coisas que precisa de saber e considerar
- Não escolhemos a proposta apenas com base no seu perfil; escolhemos principalmente com base no conteúdo da sua proposta.
  - Se incluir informações pessoais, inidique claramente quais podemos utilizar publicamente e quais não podemos utilizar. Recomendamos que limite essas informações ao que for necessário para ajudar-nos a escolher a proposta, à designação pela qual pretende ser reconhecido(/a) no evento e informações que sejam úteis e necessárias para a organização poder facilitar a sua participação na Ubucon Portugal 2023.
- O que pretende transmitira através da sua sessão, o que gostaria de partilhar?
  - A duração da sua sessão é limitada. Escreva a sua proposta de forma concreta, para que possa apresentar o conteúdo num determinado período de tempo.
- Não apresente um grande número de propostas de uma só vez. Isso não aumenta a possibilidade da sua proposta ser aceite. Podemos considerar isso como "spam" e rejeitar todas as suas propostas. Em vez disso, como tentamos convidar o maior número possível de oradores, concentre os seus esforços na redacção de uma única proposta de qualidade.

### Discussão sobre a sua proposta
Após a apresentação da proposta, podem ser realizadas discussões e reuniões pormenorizadas sobre o conteúdo da proposta até ser tomada uma decisão final. Se apresentar a sua proposta com alguma margem de manobra, pode melhorar a proposta através de consultas e reuniões com a equipa organizadora.
{{% /accordion %}}

{{% accordion title="Processo de revisão" id="tab6" expanded="true" hidden="false" %}}
Utilizaremos o seguinte processo para a análise das propostas. As informações relacionadas serão fornecidas por correio electrónico ou publicadas no nosso sítio Web.

1. Depois de apresentar a sua proposta, verificaremos se a sua proposta contém informações suficientes e enviaremos o devido retorno por correio electrónico, se necessário.
2. Quando o convite à apresentação de propostas terminar, começaremos a analisar as suas propostas. Se precisarmos de confirmar alguns detalhes, poderemos contactá-lo por correio electrónico.
3. Depois de concluída a análise, será notificado por correio electrónico se a sua proposta foi aceite.
4. Se a sua proposta for aceite, será confirmado como orador, depois de confirmar a sua participação, respondendo ao nosso contacto.
{{% /accordion %}}