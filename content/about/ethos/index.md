---
title: "Ethos"
description: "Código de conduta, regras, princípios e políticas da Ubucon Portugal 2023"
---

A Ubucon Portugal 2023 é regida por regras; elas existem para garantir a segurança e o bem-estar de todos, proporcionando a melhor experiência possível.

Sendo um evento da Comunidade Ubuntu Portugal e sendo esta comunidade aderente ao [Código de Conduta do Ubuntu](https://ubuntu.com/community/ethos/code-of-conduct), este deve ser observado por todos, pelo que convidamos à sua leitura atenta.

Além do Cógido de Conduta do Ubuntu, também nos regimos pela sua [política de diversidade](https://ubuntu.com/community/ethos/diversity) e pela sua [missão](https://ubuntu.com/community/ethos/mission).

Como a Ubucon Portugal 2023 se enquadra dentro da Festa do Software Livre, todas as regras desse evento são também aplicáveis e pedimos a todos que sigam as indicações da sua organização. Podem saber mais no site da [Festa do Software Livre 2023](https://festa2023.softwarelivre.eu/).

Além de nos enquadrarmos na Festa do Software Livre, somos também generosamente acolhidos pela [Universidade de Aveiro (UA)](https://www.ua.pt/), em particular pelo seu [Departamento de Eletrónica, Telecomunicações e Informática (DETI)](https://www.ua.pt/pt/deti) por isso, importa também ter em consideração a nossa condição de convidados e por conseguinte, respeitar as indicações dos funcionários e representantes do DETI e UA, de modo a fazermos uma utilização consciente e respeitosa do espaço que usufruíremos.

A respeito de privacidade, um tema que também nos é bastante caro, temos uma política de privacidade ([podem consultar aqui](/privacy-policy/)), que explica como gerimos a privacidade dos visitantes do nosso sítio Web e participantes do evento.