---
title: "Sobre"
description: "O que é a Ubucon Portugal 2023"
---

![Logotipo da Ubucon Portugal 2023: Um galo estilizado, sugerindo a textura de um "origami" e em tons de cor-de-laranja e violeta, com o nome do evento na sua parte inferior](/images/Logo.horizontal.svg "Logotipo da Ubucon Portugal 2023")

A Ubucon Portugal é uma conferência inteiramente organizada por e para a comunidade para celebrar o Ubuntu, organizada pela [Ubuntu-pt - Comunicade Ubuntu Portugal](https://ubuntu-pt.org "Visitar") em colaboração com várias comunidades Portuguesas.

A 2ª edição da Ubucon Portugal será realizada na Universidade de Aveiro. Junte-se a nós em 17 de Setembro deste ano para conhecer a comunidade Portuguesa de Ubuntu e a vibrante comunidade de Software Livre e Software de Código Aberto nacional!
