---
title: "Há alojamento gratuito para estudantes virem à Ubucon Portugal"
date: 2023-09-06T17:00:00+01:00
authors:
    - name: Miguel Gomes
      bio: Organizador, Ubucon Portugal 2023 / Membro do Conselho Comunitário, Ubuntu Portugal
      email: mgc.lude@protonmail.ch
      launchpad: mgc.lude
      github: 
      gitlab: mgc.lude
      profile: https://gitlab.com/uploads/-/system/user/avatar/11423003/avatar.png
      linkurl: https://mastodon.social/@per_sonne
      linklabel: Mastodon
---

# Há alojamento gratuito para estudantes virem à Ubucon Portugal

Graças à [Associação Nacional para o Software Livre - ANSOL](https://ansol.org), está disponível alojamento gratuito em Aveiro, para os estudantes que o solicitem, durante os dias da [Festa do Software Livre 2023](https://festa2023.softwarelivre.eu/). Assim, os estudantes que pretendam visitar a Ubucon e/ou Festa do Software Livre 2023 têm aqui a oportunidade de beneficiar de estadia. Não há desculpas para não vir!

Para reservar uma cama, basta que enviem um e-mail para [contacto@ansol.org](contacto@ansol.org) a dizer que são estudantes e que querem alojamento. As vagas estão limitadas ao número de camas disponíveis, por isso, não percam tempo!