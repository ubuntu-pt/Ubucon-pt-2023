---
title: "Contacto para Privacidade"
description: "Informação sobre contacto para dúvidas e exercício de Direitos sobre Dados Pessoais"
---

Se deseja contactar a organização da Ubucon Portugal 2023, com dúvidas sobre questões de privacidade deste sítio Web, questões de privacidade no próprio evento, ou exercício de Direitos legais sobre os seus dados pessoais, pode [utilizar este formulário](https://hub.ubuntu-pt.org/apps/forms/s/HkBDYNWeBYTNzGyrtJKmKK4M).

Não se esqueça também de consultar a nossa ([política de privacidade](/privacy-policy/)).