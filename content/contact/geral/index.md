---
title: "Contacto"
description: "Formulário de contacto para fins gerais"
---



Se deseja contactar a organização da Ubucon Portugal 2023, com dúvidas ou algum tipo de pedido pode [utilizar este formulário](https://hub.ubuntu-pt.org/apps/forms/s/wZNEsYqwoeqY8YjjAL5LfkzF).